#!/bin/bash
#
# Generates a zip files containing 10 times all possible classification boards layouts
#
# The distribution of boards with respect to the given level (l) is given by:
# l init-max (number) possible
# 1    3-3   (1)      4
# 2    5-9   (3)      
# 3    7-27  (11)
# 4    9-81  (37)
#
# I should have computed this...
# (init=2l+1) nombre minimal de feuilles à un niveau
# (max=3^l) nombre maximal de feuilles à un niveau
# (number=(max-init)/2+1)

#for l from 1 to 4
#    for j from init(l) to max(l)
#	10 times: GenBoad -p l j

DIRNAME=classboards

# Initial function not used but could
# Generates around 500 boards (10 per level*leaves pair)
function gen500BoardsByLevels() {
    for l in {1..4}
    do
	# Here we do it modulo 2n+1
	max=$(((3**${l}-1)/2))
	for (( j=${l}; j<=${max}; j++ ))
	do
	    for i in {1..10}
	    do
		java GenBoard -p ${l} $((${j}*2+1))
	    done
	done
    done
}

# 1 number
# 2 level
# 3 start
# 4 end
# generates $1 boards of level $2 randomly chosen between $3 and $4
function genBoardByLevel() {
    for (( j=1; j<=${1}; j++ ))
    do
	# Here we do it modulo 2n+1
	beg=$(((${3}-1)/2))
	end=$(((${4}-1)/2))
	# draw a random number (if necessary)
	if [ $beg == $end ]
	then
	    leaves=$beg
	else
	    dif=$(( end - beg + 1 ))
	    leaves=$(( beg + RANDOM % dif ))
	fi
	# generate
	java GenBoard -p ${2} $((${leaves}*2+1))
    done
}

# Generates around 50 boards better distributed by levels
genBoardByLevel 10 1 3 3 # generates only 4 anyway
genBoardByLevel 10 2 5 9
genBoardByLevel 15 3 7 19
genBoardByLevel 5 3 21 27
genBoardByLevel 10 4 9 21
genBoardByLevel 5 4 23 79
genBoardByLevel 1 4 81 81

mkdir /tmp/${DIRNAME}
mv /tmp/*.pdf /tmp/*.png /tmp/${DIRNAME}/

# Generating a HTML file
echo "<html>" > /tmp/${DIRNAME}/index.html
echo "<head><title></title></head>" >> /tmp/${DIRNAME}/index.html
echo "<body>" >> /tmp/${DIRNAME}/index.html
echo "<a href=\"https://moex.inria.fr/mediation/class/\"><i>Class?</i></a>" >> /tmp/${DIRNAME}/index.html
echo "<h1>Library of classification boards</h1>" >> /tmp/${DIRNAME}/index.html
echo "<p>Boards are ordering in supposedly increasing difficulty</p>" >> /tmp/${DIRNAME}/index.html
echo "<table style=\"border: none; text-align: center;\"><tr><td><b>height</b></td><td><b>leaves</b></td><td><b>file</b></td></tr>" >> /tmp/${DIRNAME}/index.html
echo "" >> /tmp/${DIRNAME}/index.html
for f in `ls /tmp/${DIRNAME}/*.png|sed "s:/tmp/${DIRNAME}/\([^.]*\).png:\1:"|sort -n -t '-' -k 2,2 -k 1,1`
do
    ar=(${f//-/ })
    echo "<tr><td>${ar[1]}</td><td>${ar[0]}</td><td><a href=\"${f}.png\">PNG</a> | <a href=\"${f}.pdf\">PDF</a></td></tr>" >> /tmp/${DIRNAME}/index.html
done
echo "</table>" >> /tmp/${DIRNAME}/index.html
echo "<hr /><small>Generated on "`date +%Y-%m-%d`"</small>" >> /tmp/${DIRNAME}/index.html
echo "</table>" >> /tmp/${DIRNAME}/index.html
echo "</body>" >> /tmp/${DIRNAME}/index.html
echo "</html>" >> /tmp/${DIRNAME}/index.html

cd ..; zip -r ${DIRNAME}.zip ${DIRNAME}

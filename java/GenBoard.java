/**
 * GenBoard: board generation 
 *
 * Distributed under GNU LGPL License version 3
 * (C) INRIA, 2020
 *
 * TODO:
 * - class testng
 * - servlet
 */

/*
$ java GenBoard 1 3451
The number of leaves has to be an odd integer between 1 and 81
$ java GenBoard 1 81  
 CALL: l=1; l=81
Cannot reach 81 leaves with 1 levels
$ java GenBoard 1 82
The number of leaves has to be an odd integer between 1 and 81
$ java GenBoard 1 83
The number of leaves has to be an odd integer between 1 and 81

$ java GenBoard 4 81
====> error
$ java GenBoard 2 7
 (filling (color ()  ()  () )  ()  (color ()  ()  () ) ) 
====> error
$ java GenBoard 2 9
 CALL: l=2; l=9
     Fork> m=-3.0; m=3
 CALL: l=1; l=5
====> error
$ java GenBoard 2 9
---> bloc/loop
$ java GenBoard 1 3
 (color ()  ()  () )
$ java GenBoard 2 3
---> bloc/loop
 */

// package fr.inria.moex.class.
// Split GenBoard -> RandomBoardGenerator and Tree -> Classification
import java.util.Random;

import java.lang.ProcessBuilder;
import java.lang.Process;

import java.io.PrintStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.File;

public class GenBoard {

    static int NBFEATURES = 4;
    static int NBVALUES = 3;
    static int MAXLEAVES = (int)Math.pow( NBVALUES, NBFEATURES );

    static Random generator;

    static boolean LATEX = false;
    static boolean PNG = false;
    
    /**
     * GenBoard OPT level leaves
     * generates a classification board of at most levels and exactly leaves.
     * OPT:
     * -e: reach exactly levels
     * -l: generate LaTeX for the board
     * -p: compile the LaTeX into PNG
     */ 
    public static void main(String[] args) {
	int level = -1;
	int leaves = -1;
	boolean exact = false;

	// parse command line arguments
	if ( args.length < 2 ) {
	    System.err.println( "Need at least two arguments: level and leaves" );
	    System.exit(1);
	}
	for ( int i=0; i < args.length; i++ ) {
	    String arg = args[i].trim();
	    if ( arg.equals( "-e") ) {
	        exact = true;
	    } else if ( arg.equals( "-l") ) {
		LATEX = true;
	    } else if ( arg.equals( "-p") ) {
		LATEX = true;
		PNG = true;
	    } else {
		if ( level == -1 ) {
		    level = Integer.parseInt( arg );
		} else {
		    leaves = Integer.parseInt( arg );
		}
	    }
	}

	// check that leaves are possible
	if ( level < 1 || level > NBFEATURES ) {
	    System.err.println( "The number of levels should be an integer between 1 and "+NBFEATURES );
	    System.exit(1);
	} else if ( leaves < 1 || (leaves & 1) == 0 || leaves > MAXLEAVES ) {
	    System.err.println( "The number of leaves has to be an odd integer between 1 and "+MAXLEAVES );
	    System.exit(1);
	} else if ( leaves > Math.pow( NBVALUES, level ) ) {
	    System.err.println( "Cannot reach "+leaves+" leaves with "+level+" levels" );
	    System.exit(2);
	} else if ( exact && ( leaves < level*(NBVALUES-1)+1 ) ) {
	    System.err.println( "Cannot reach exactly "+level+" levels with "+leaves+" leaves" );
	    System.exit(2);
	}

	Tree result = genBoard( level, leaves, exact );
	
	if ( LATEX ) {
	    result.printLaTexTree( level, leaves );
	} else {
	    result.printTree();
	    System.out.println();
	}
    }

    public static Tree genBoard( int levels, int leaves, boolean exact ) {
	generator = new Random();

	int nbforks = (leaves-1)/(NBVALUES-1);
	return new GenBoard().drawTree( levels, nbforks, new boolean[NBFEATURES], exact );
    }

    // generates a Tree structure of at most (or exactly) levels, with forks.
    // taken is the set of forbiden characteristics to be choosen
    protected Tree drawTree( int level, int forks, boolean[] taken, boolean exact ) {
	//System.err.println( " CALL: l="+level+"; q="+forks );
	Tree result = null;
	if ( forks == 0) {
	    result = new Tree( 0 );
	} else {
	    // draw spliting feature
	    int feature = generator.nextInt(NBFEATURES);
	    while ( taken[feature] ) feature = generator.nextInt(NBFEATURES);
	    taken[feature] = true;
	    // draw one branch to reach the requested level
	    int maxBranch = exact?generator.nextInt(NBVALUES):-1;
	    // build subtrees
	    result = new Tree( feature+1 );
	    int maxSubForks = nbForks( level );
	    int rest = forks-1;
	    for ( int i=0; i < NBVALUES; i++ ) {
		int nbforks = 0;
		if ( !exact || i>maxBranch ) {
		    nbforks = drawForks( Math.max(0,rest-((NBVALUES-(i+1))*maxSubForks)), Math.min(maxSubForks,rest) );
		} else if ( exact && i==maxBranch ) {
		    nbforks = drawForks( Math.max(level-1,rest-((NBVALUES-(i+1))*maxSubForks)), Math.min(maxSubForks,rest) );
		} else { // before maxBranch save level-1 forks from max
		    nbforks = drawForks( Math.max(0,rest-((NBVALUES-(i+1))*maxSubForks)), Math.min(maxSubForks,rest-level+1) );
		}
		result.subTree[i] = drawTree( level-1, nbforks, taken, (i==maxBranch)?true:false );
		rest -= nbforks;
	    }
	    taken[feature] = false;
	}
	return result;
    }

    // Maximum number of forks of a tree with level levels
    protected int nbForks( int level ) {
	int result = 0;
	for ( int i=1; i < level; i++ ) {
	    result += (int)Math.pow( NBVALUES, i-1 );
	}
	return result;
    }
    // draw a random integer between min and max
    protected int drawForks( int minforks, int maxforks ) {
	int result = minforks+generator.nextInt( maxforks-minforks+1 );
	return result;
    }
}

/**
 * Structure of the Classication board
 */
class Tree {

    public int feature = 0;

    public Tree[] subTree = null;

    public Tree ( int f ) {
	feature = f;
	if ( f != 0 ) {
	    subTree = new Tree[GenBoard.NBVALUES];
	}
    }

    public String treeName( int levels, int leaves ) {
	long code = toString().hashCode() - (long)java.lang.Integer.MIN_VALUE;
	return leaves+"-"+levels+"-"+code;
    }

    public String toString() {
	String result = feature+"";
	if ( feature != 0 ) {
	    for ( Tree sub : subTree ) {
		result += sub.toString();
	    }
	}
	return result;
    }

    // So far everything is generic (features/values are just numbers)
    // Below the output functions are specific to the Class? game

    // Print the board as a lisp-like structure indicating only the characteristics
    public void printTree() {
	System.out.print( " (" );
	switch (feature) {
	case 0: break;
	case 1: System.out.print( "color" ); break;
	case 2: System.out.print( "shape" ); break;
	case 3: System.out.print( "filling" ); break;
	case 4: System.out.print( "number" ); break;
	}
	if ( feature != 0 ) {
	    for ( Tree sub : subTree ) {
		sub.printTree();
	    }
	}
	System.out.print( ") " );
    }

    // JE: all these could be computed once and for all (because here it is computed many times)
    // interspace (in points?)
    public int intersp = 4; // cardmininterval??
    public int leaveWidth = 10; // size of normal?

    // The sibling distance
    public int leftSpan() {
	if ( feature == 0 ) return 0; // never called on these
	return subTree[0].rightWidth()+intersp+subTree[1].leftWidth();
    }

    public int rightSpan() {
	if ( feature == 0 ) return 0; // never called on these
	return subTree[1].rightWidth()+intersp+subTree[2].leftWidth();
    }

    // The space between branches
    public int leftWidth () {
	if ( feature == 0 ) return leaveWidth/2;
	return leftSpan()+subTree[0].leftWidth();
    }
    
    public int rightWidth () {
	if ( feature == 0 ) return leaveWidth/2;
	return rightSpan()+subTree[2].rightWidth();
    }
    
    // Generate the LaTeX corresponding to the tree
    public void printLaTexTree( int level, int leaves ) {
	PrintStream stdout = System.out;
	PrintStream ps = null;
	String treeName = treeName( level, leaves );
	File texFile = null;
	try {
	    if ( GenBoard.PNG ) {
		// Open temporary file
		texFile = new File( System.getProperty("java.io.tmpdir"), treeName+".tex" );
		// Set System.out to it
		//ps = new PrintStream(new BufferedOutputStream(new FileOutputStream("toto.tex")), true);
		ps = new PrintStream(new BufferedOutputStream(new FileOutputStream(texFile)), true);
		System.setOut( ps );
	    }

	    // multi={tikzpicture}, create -0 files
	    System.out.println( "\\documentclass[convert]{standalone}");
	    System.out.println();
	    System.out.println( "\\usepackage{url}");
	    System.out.println();
	    System.out.println( "\\usepackage[bundle=class]{classdeck}");
	    System.out.println();
	    System.out.println( "\\begin{document}");
	    System.out.println();
	    System.out.println( "\\begin{tikzpicture}[scale=1.1,edge from parent fork down,");
	    System.out.println( "      level/.style={sibling distance=\\cardmininterval},");
	    System.out.println( "      level distance=2cm,");
	    System.out.println( "      every node/.append style={inner sep=0,outer sep=0}]");
	    System.out.print( "\\");
	    printTikZ( "0", "\\jokerclasspattern", "\\jokerclasscolor", "\\jokerclassshape" );
	    System.out.println( ";");
	    System.out.println();
	    System.out.print( "% ");
	    printTree();
	    System.out.println();
	    System.out.println( "  \\draw (current bounding box.north west) node[anchor=north west] {\\tiny\\url{https://moex.inria.fr/mediation/class/}};");
	    System.out.println( "  \\draw (current bounding box.north east) node[anchor=north east] {\\tiny  $"+treeName.replaceFirst("-", "/")+"$};");
	    System.out.println( "\\end{tikzpicture}");
	    System.out.println( "\\end{document}");
	} catch (Exception ex) {
	    ex.printStackTrace();
	} finally { // Close it
	    System.setOut( stdout );
	    if ( GenBoard.PNG ) { ps.close(); } // unclear
	}
	if ( GenBoard.PNG ) {
	    // Compile it using Runtime
	    try { 
		// Command to create an external process
		File outDir = texFile.getParentFile();
		ProcessBuilder pb = new ProcessBuilder().inheritIO();
		pb.command( "pdflatex", "-shell-escape", texFile.toString() ); // -output-directory="+outDir+"
		pb.directory( outDir );
		Process proc = pb.start();
		proc.waitFor();
		System.out.println( "open "+outDir+"/"+texFile.getName().replaceFirst("[.][^.]+$", "")+".png" );
	    } catch (Exception e) { 
		e.printStackTrace(); 
	    }
	}
    }

    // Recursive printing of a subtree
    public void printTikZ( String number, String pattern, String color, String shape ) {
	switch (feature) {
	case 0:
	    System.out.print( "node {\\classpict{finalclasssize}{"+number+"}{"+pattern+"}{"+color+"}{"+shape+"}}");
	    break;
	case 1:
	    System.out.print( "node {\\classpict{smallclasssize}{"+number+"}{"+pattern+"}{"+color+"}{"+shape+"}}");
	    System.out.print( "\nchild[sibling distance="+leftSpan()+"mm] {" ); subTree[0].printTikZ( number, pattern, "\\redclasscolor", shape ); System.out.print( "}");
	    System.out.print( "\nchild {" ); subTree[1].printTikZ( number, pattern, "\\blueclasscolor", shape ); System.out.print( "}");
	    System.out.print( "\nchild[sibling distance="+rightSpan()+"mm] {" ); subTree[2].printTikZ( number, pattern, "\\greenclasscolor", shape ); System.out.print( "}");
	    break;
	case 2:
	    System.out.print( "node {\\classpict{smallclasssize}{"+number+"}{"+pattern+"}{"+color+"}{"+shape+"}}");
	    System.out.print( "\nchild[sibling distance="+leftSpan()+"mm] {" ); subTree[0].printTikZ( number, pattern, color, "\\firstclassshape" ); System.out.print( "}");
	    System.out.print( "\nchild {" ); subTree[1].printTikZ( number, pattern, color, "\\secondclassshape" ); System.out.print( "}");
	    System.out.print( "\nchild[sibling distance="+rightSpan()+"mm] {" ); subTree[2].printTikZ( number, pattern, color, "\\thirdclassshape" ); System.out.print( "}");
	    break;
	case 3:
	    System.out.print( "node {\\classpict{smallclasssize}{"+number+"}{"+pattern+"}{"+color+"}{"+shape+"}}");
	    System.out.print( "\nchild[sibling distance="+leftSpan()+"mm] {" ); subTree[0].printTikZ( number, "\\emptyclasspattern", color, shape ); System.out.print( "}");
	    System.out.print( "\nchild {" ); subTree[1].printTikZ( number, "\\fillingclasspattern", color, shape ); System.out.print( "}");
	    System.out.print( "\nchild[sibling distance="+rightSpan()+"mm] {" ); subTree[2].printTikZ( number, "\\fullclasspattern", color, shape ); System.out.print( "}");
	    break;
	case 4:
	    System.out.print( "node {\\classpict{smallclasssize}{"+number+"}{"+pattern+"}{"+color+"}{"+shape+"}}");
	    System.out.print( "\nchild[sibling distance="+leftSpan()+"mm] {" ); subTree[0].printTikZ( "1", pattern, color, shape ); System.out.print( "}");
	    System.out.print( "\nchild {" ); subTree[1].printTikZ( "2", pattern, color, shape ); System.out.print( "}");
	    System.out.print( "\nchild[sibling distance="+rightSpan()+"mm] {" ); subTree[2].printTikZ( "3", pattern, color, shape ); System.out.print( "}");
	    break;
	}
    }
}

# Class?

LaTeX package for a card game used to illustrate social knowledge evolution

## What is it?

We designed a card game for illustrating our work (https://moex.inria.fr/mediation/class/).
This package offer the possibilty to print cards and boards.

It can be used with its own set of cards or with cards from the Set! game (https://www.setgame.com).

## How do I use it?

Simply add the package in the .tex file

    \usepackage[bundle=bundlename]{classdeck}

with the bundlename being either `class` (default), `setline` or `setjerome`.
These define the default layout.

Then it is possible to use:

    \classpict{mediumclasssize}{2}{\emptyclasspattern}{\redclasssolor}{\secondclassshape}
  
in LaTeX, or:

    \classcard{mediumclasssize}{0}{\jokerclasspattern}{\jokerclasscolor}{\jockerclassshape}{0}{0}

in tikZ.

The fuller documentation is available in the classdeck.tex file.

## What's in?

- classdeck.sty: a LaTeX package providing various ways to layout cards for the game
- classdeck.tex: documentation of the set

## Board generator in Java

We added to this package a random board generator written in Java (in the java directory).

It may be compiled by:

    $ javac GenBoard.java

and run by:

    $ java GenBoard 3 17
     (number (color (filling ()  ()  () )  (shape ()  ()  () )  () )  ()  (filling (color ()  ()  () )  (color ()  ()  () )  (shape ()  ()  () ) ) ) 

The first argument is the maximum number of levels and the second one the desired number of leaves.

This function may take other arguments:

* -l: for generating a LaTeX board (using classdeck)
* -p: the generation of a PNG file from the LaTeX 

It is used by the genZIP.sh script which can generates batches of random boards.

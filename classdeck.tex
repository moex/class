\documentclass[a4paper]{article}

% Bloody usefull for having all well displayed
\usepackage[margin=1.25in, headheight=14pt]{geometry}
\usepackage{graphicx}
\usepackage{rotating}
\usepackage{afterpage}
\usepackage{url}
\usepackage{hyperref}
\usepackage{xspace}
\usepackage{listings}

%\lstset{language=latex}
\lstset{basicstyle=\small\ttfamily, basewidth=0.51em,escapeinside=~~}

\usepackage{latexsym}

% Create errors!
\usepackage[inline]{enumitem}

%\lstset{language=latex}
\lstset{basicstyle=\small\ttfamily, basewidth=0.51em,escapeinside=~~}

\setlist{leftmargin=.5cm,topsep=0pt,parsep=-5pt}
\setlist[2]{itemsep=4pt}
\setlist[description]{font={\normalfont\sffamily}}

\newlist{enuminline}{enumerate*}{5}
\setlist[enuminline]{label={(\arabic*)}}

\newlist{enumii}{enumerate*}{5}
\setlist[enumii]{label={(\emph{\roman*})}}

\newlist{enumaa}{enumerate*}{5}
\setlist[enumaa]{label={(\emph{\alph*})}}

\def\labelitemi{--}

% it seems that if I move this after PGF, then it breaks
% because it requires xcolor with dvipsnames loaded...
\usepackage[bundle=class]{classdeck}

\usetikzlibrary{calc}

\def\gamename{\textit{Class?}\xspace}

\title{\LaTeX\xspace \texttt{classdeck} documentation}
\date{\today}
\author{Jérôme Euzenat}

\begin{document}

\maketitle

\begin{abstract}
The \texttt{classdeck} package provides commands to draw cards and board for the \gamename game.
It can be used as well for the \textit{Set!} get of which it is a superset.
\end{abstract}

In order to develop the \gamename
% (or maybe ``Edge'')
game\textit{Set!}\footnote{\url{https://moex.inria.fr/mediation/class/}}, we took inspiration from the TikZ package for the \textit{Set!}\footnote{\url{https://setgame.com}} game developed by Gwyn Whieldon in 2012\footnote{\url{https://www.ctan.org/pkg/setdeck}}.
Currently, what remains of Gwyn's style is the spirit and the \texttt{vertical stripes} (see \S\ref{sec:filling}).

We first describe the possible card features (\S\ref{sec:features}): shape, number, colours and patterns.
Then we consider some graphical attributes of the drawings (\S\ref{sec:graphic}: size, orientation, contours) before providing convenient macros for drawing cards (\S\ref{sec:cards}).
We introduce bundles which are sets of consistent groups of features that are usually implemented in one game material (\S\ref{sec:bundle}).
Finally, some example of the use and associated utilities are presented (\S\ref{sec:util}).
In particular, they allow to draw \gamename classifications.

\section{\gamename card features}\label{sec:features}

\gamename cards are displayed with respect to some features of objects: shape, number, colors, fillings.
For each features, there are three possible values.
We show below sets of consistent values.

In addition, \gamename has one joker value representing the absence of knowledge about a feature value.
We show below the possible values for each features.

\subsection{Shapes}\label{sec:shapes}

Here are some layouts for available original \textit{Set!} cards:

\begin{tabular}{cc}
\includegraphics[width=8cm]{imgs/original-set.png} &
                                                \includegraphics[width=5cm]{imgs/set-example.png} \\
  original & other layout
\end{tabular}


Here are the available consistent sets of shapes:
\begin{center}
  \setlength{\tabcolsep}{5pt}
  \begin{tabular}{cccc}
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{circle} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{triangle} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{square} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{star8} \\
    circle & triangle & square & star8\\
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{wurst} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{diamond} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{peanut} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{trapez} \\
    wurst & diamond & peanut & trapez\\
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{rect} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{oval} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{squiggle} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{ticket} \\
    rect & oval & squiggle & ticket \\ 
    % \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{rect} &
    % \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{losange} &
    % \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{squiggle} &
    % \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{ticket} \\
    % clubs & hearts & spades & ticket \\ 
\end{tabular}
\end{center}

In the default bundle, these shapes are called \verb|\firstclassshape|, \verb|\secondclassshape|, \verb|\thirdclassshape|, and \verb|\jokerclassshape|.

\subsection{Numbers}\label{sec:numbers}

The numbers are classically 1, 2 or 3.
The joker value has been arbitrarily set to 0 (it is displayed as 5 elements).

\subsection{Colors}\label{sec:colors}

Here are available sets of colors. The joker color is in principle set to gray.

In the default bundle, these colors are called \verb|\redclasscolor|, \verb|\blueclasscolor|,  \verb|\greenclasscolor| and \verb|\jokerclasscolor|.
Here are a sample of their values:

\begin{center}
  \setlength{\tabcolsep}{5pt}
  \begin{tabular}{cccc}
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{oval} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\blueclasscolor}{oval} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{\greenclasscolor}{oval} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{Gray}{oval} \\
    Red & NavyBlue & DarkPastelGreen & Gray\\
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{Red}{oval} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{PurpleHeart}{oval} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{OliveGreen}{oval} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{Gray}{oval} \\
    Red & PurpleHeart & OliveGreen & Gray\\
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{Lava}{oval} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{Regalia}{oval} & %RoyalPurple
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{ForestGreen}{oval} &
    \classpict{mediumclasssize}{\oneclassnumber}{\fullclasspattern}{Gray!80}{oval} \\
    Lava & Regalia & ForestGreen & Gray!80\\
\end{tabular}
\end{center}

In addition, we preserved the initial convention of \texttt{setdeck} to define three variants of these colors: bordercolor, stripecolor (for the \verb|\fillingclasspattern|), fillcolor (for the \verb|\fullclasspattern|)
They were defined as:
\begin{description}
 \item[Red] Red, Red!90, Red!40
 \item[Blue] RoyalPurple, RoyalPurple!90, RoyalPurple!40
 \item[Green] OliveGreen, OliveGreen!90, PineGreen!40
\end{description}

%can't we use colorlet?

\subsection{Patterns}\label{sec:filling}\label{sec:patterns}

Filling is characterised by patterns corresponding to either no filling, full filling or a pattern filling.
The joker filling is also a pattern.
In the default bundle, these patterns are called \verb|\emptyclassfilling|, \verb|\stripedclassfilling|, \verb|\fullclassfilling| and \verb|\jokerclassfilling|.

Here are already some patterns defined in PGF (which can be used as such):
\begin{center}
  \setlength{\tabcolsep}{0pt}
  \begin{tabular}{cccccc}
    \classpict{mediumclasssize}{\oneclassnumber}{vertical lines}{\redclasscolor}{oval} &
    \classpict{mediumclasssize}{\oneclassnumber}{thin vertical stripes}{\redclasscolor}{oval} &
    \classpict{mediumclasssize}{\oneclassnumber}{vertical stripes}{\redclasscolor}{oval} &
    \classpict{mediumclasssize,ultra thick}{\oneclassnumber}{vertical lines}{\redclasscolor}{oval} &
    \classpict{mediumclasssize}{\oneclassnumber}{crosshatch}{\redclasscolor}{oval} &
    \classpict{mediumclasssize}{\oneclassnumber}{crosshatch dots}{\redclasscolor}{oval} \\
  vertical lines & thin vertical stripes & vertical stripes & vertical lines & crosshatch & crosshatch dots \\
\end{tabular}
\end{center}

Such patterns are defined as PGF patterns.
So actually, the line drawing attributes (\verb|thin|, \verb|thick|, \verb|very thick|, etc.) do not apply to patterns.
These are written in pgf and they rely on the size of the initial ``point''.

Moreover, it is noticeable that patterns are not easily rotated (or not rotated at all).
This is a feature of pgf/tikz that makes it fast to render.
Hence so far, when a card is rotated, the stripes keep their initial orientation... 

\section{Size, form and orientation of cards}\label{sec:graphic}

Drawing cards goes beyond their features. They may be drawn in a variety of size, orientation, shapes and border.

\subsection{Square or rectangular}

Card may be square or rectangular.

So far these features are tied to bundles and cannot be easily modified (see \S\ref{sec:bundle}).

\subsection{Orientation}\label{sec:orientation}

This is a difficult subject because orientation may mean two things:
it is possible to orientate rectangular cards with the large side horizontal or vertical.
Vertical is a classical way to hold cards, horizontal provides a better filling in case of horizontal shapes;
It is also possible to rotate the cards for printing a tree vertically for instance (see \S\ref{sec:trees}).
(but the vertical stripes remain vertical, see \S\ref{sec:patterns}).

We use the first optional card macro parameter to specify orientation (see \S\ref{sec:cards}).

This also shows that there is lattitude for eventually deciding the orientation.

This is not difficult, but the orientation must be passed to the whole element, a scope rotation does not work.

\subsection{Size}\label{sec:size}

Several sizes of cards may be drawn by scaling and adapting graphic attributes.

It seems that obtaining small cards can simply be obtained by using \verb|thin| instead of \verb|thick| or \verb|very thick|.

We use the first optional card macro parameter to specify size (see \S\ref{sec:cards}).
It can be made of three different elements:
\begin{itemize}
\item the scale (\texttt{scale=.5})
\item the size of strokes (\verb|thin|, \verb|thick|, \verb|very thick|)
\item the type of stripes as defined in setdeck (vertical stripes, thin vertical stripes)
\end{itemize}

We have defined five packages that can be used as this first parameter:
% Note, instead of thin... it might be possible to use line width=
\begin{itemize}
\item\texttt{tinyclasssize = thin,scale=.2} (+ \texttt{thin vertical stripes})
\item\texttt{smallclasssize = thin,scale=.25} (+ \texttt{thin vertical stripes})
\item\texttt{finalclasssize = thin,scale=.30} (+ \texttt{thin vertical stripes})
\item\texttt{mediumclasssize = very thick,scale=.5}
\item\texttt{largeclasssize = very thick,scale=1}
\item\texttt{largerclasssize = ultra thick,scale=1.2} is the size of printed cards (but may be increased)
\end{itemize}

Here is the result for each of these classes:

\begin{center}
  \setlength{\tabcolsep}{10pt}
  \begin{tabular}{ccccc}
    \classpict{tinyclasssize}{\oneclassnumber}{thin vertical stripes}{\redclasscolor}{oval} &
    \classpict{smallclasssize}{\oneclassnumber}{thin vertical stripes}{\redclasscolor}{oval} &
    \classpict{finalclasssize}{\oneclassnumber}{thin vertical stripes}{\redclasscolor}{oval} &
    \classpict{mediumclasssize}{\oneclassnumber}{thin vertical stripes}{\redclasscolor}{oval} &
    \classpict{largeclasssize}{\oneclassnumber}{thin vertical stripes}{\redclasscolor}{oval}
    %  & \classpict{largerclasssize}{\oneclassnumber}{}{\redclasscolor}{oval}
    \\
    tiny & small & final & medium & large
    % & larger
    \\
\end{tabular}
\end{center}

\subsection{Borders}

It is possible to switch card borders by redefining the variable \verb|\classcontour| or by setting the value of the \verb|outline| package parameter (at the level of the whole document) to:
\begin{itemize}
\item \texttt{outline} (default): displays card ouline as in most examples in this document;
\item \texttt{nocontour}: does not display any border;
\item \texttt{guideline}: does display marks for cutting paper around the card.
\end{itemize}

The result is:

\begin{center}
  \setlength{\tabcolsep}{10pt}
  \begin{tabular}{ccc}
    \classpict{mediumclasssize}{\oneclassnumber}{thin vertical stripes}{\redclasscolor}{oval} &
    {  \def\classcontour{noborder}
\classpict{mediumclasssize}{\oneclassnumber}{thin vertical stripes}{\redclasscolor}{oval}} &
    {  \def\classcontour{guideline}
\classpict{mediumclasssize,ultra thick}{\oneclassnumber}{thin vertical stripes}{\redclasscolor}{oval}}
    \\
    outline & noborder & guideline
    % & larger
    \\
\end{tabular}
\end{center}


\section{Card macros}\label{sec:cards}

We introduce two commands for displaying cards.

\noindent\verb|\classcard[{<ScaleParams>}]{<Number>}{<Filling>}{<Color>}{<Shape>}{<X>}{<Y>}|.

and

\noindent\verb|\classpict{<ScaleParams>}{<Number>}{<Shading>}{<Color>}{<Shape>}| or\\
\noindent\verb|\classtext{<Number>}{<Shading>}{<Color>}{<Shape>}|.

The former is usable in tikz and draws the class at the X and Y (should add Z) coordinates; the latter puts the \texttt{classcard} within a \texttt{tikzpicture}.
Hence, it can be inserted anywhere like normal \LaTeX\xspace commands.
This is in particular useful to insert in a tikZ node label.
\texttt{classtext} is a variation for including cards with texts (for instance here:\classtext{\twoclassnumber}{filled}{red}{square}).

\verb|\classcard| is a rewrite of \verb|\setcard|, except that, instead of passing numbers, one has to pass values which are actual numbers, pattern, color and shape.
Hence, one can use any fancy colours (however, numbers only work from 0--3 and 5).

Here is the result of \verb|\classcard{mediumclasssize}{\twoclassnumber}{crosshatch}{harvardcrimson}{losange}{0}{0}|: % #1

\begin{center}%
\definecolor{harvardcrimson}{rgb}{0.79, 0.0, 0.09}
\begin{tikzpicture}
  \classcard{mediumclasssize}{\twoclassnumber}{crosshatch}{harvardcrimson}{losange}{0}{0}
\end{tikzpicture}
\end{center}

Specific variables are defined for the default display (see \S\ref{sec:bundle}).
They are:

\begin{center}
\begin{tabular}{| c | c | c | c |}
\hline
Number & Filling & Color & Shape \\
(a number) & (a pattern) & (a color) & (a shape) \\
\hline
 0 & \verb|\jokerclasspattern| & \verb|\jokerclasscolor| & \verb|\jokerclassshape|\\ \hline
 1 & \verb|\emptyclasspattern| & \verb|\redclasscolor| & \verb|\firstclassshape| \\
 2 & \verb|\fillingclasspattern| & \verb|\blueclasscolor| & \verb|\secondclassshape| \\
 3 & \verb|\fullclasspattern| & \verb|\greenclasscolor| & \verb|\thirdclassshape| \\
\hline
\end{tabular}
\end{center}

The first line corresponds to classes.

The actual usable card set is obtained by another macro:

\begin{center}
  \begin{tikzpicture}
    \realclasscard{\twoclassnumber}{\fillingclasspattern}{\greenclasscolor}{\firstclassshape}{0}{0};
  \end{tikzpicture}
\end{center}

\section{Bundles}\label{sec:bundle}

The easier way to display a consistent set of cards is to use a bundle which specifies the default card set.
The bundle is specified by passing the \texttt{bundle} parameter when loading the package:
\begin{lstlisting}
\usepackage[bundle=<bundlename>]{classdeck}
\end{lstlisting}
or later through:
\begin{lstlisting}
\setclassbundle{<bundlename>}
\end{lstlisting}
You can change this and see the effect on this file or any other.

There are currently three bundles:
\begin{itemize}
\item class: the bundle that we designed for \gamename;
\item setline: the card layout of the \textit{Set!} game that Lise has;
\item setjerome: the apparently standard \textit{Set!} game layout.
\end{itemize}

This document has been loaded with:
\begin{lstlisting}
\usepackage[bundle=~\thebundle~]{classdeck}
\end{lstlisting}
and now you can look at the effect of:
\begin{lstlisting}
\setclassbundle{setline}
\end{lstlisting}
\begin{center}
\setclassbundle{setline}
\classpict{mediumclasssize}{\oneclassnumber}{\emptyclasspattern}{\redclasscolor}{\firstclassshape}
\classpict{mediumclasssize}{\twoclassnumber}{\fillingclasspattern}{\blueclasscolor}{\secondclassshape}
\classpict{mediumclasssize}{\threeclassnumber}{\fullclasspattern}{\greenclasscolor}{\thirdclassshape}
\classpict{mediumclasssize}{\jokerclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\jokerclassshape}
\end{center}

\begin{lstlisting}
\setclassbundle{setjerome}
\end{lstlisting}
\begin{center}
\setclassbundle{setjerome}
\classpict{mediumclasssize}{\oneclassnumber}{\emptyclasspattern}{\redclasscolor}{\firstclassshape}
\classpict{mediumclasssize}{\twoclassnumber}{\fillingclasspattern}{\blueclasscolor}{\secondclassshape}
\classpict{mediumclasssize}{\threeclassnumber}{\fullclasspattern}{\greenclasscolor}{\thirdclassshape}
\classpict{mediumclasssize}{\jokerclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\jokerclassshape}
\end{center}

\begin{lstlisting}
\setclassbundle{class}
\end{lstlisting}
\begin{center}
\setclassbundle{class}
\classpict{mediumclasssize}{\oneclassnumber}{\emptyclasspattern}{\redclasscolor}{\firstclassshape}
\classpict{mediumclasssize}{\twoclassnumber}{\fillingclasspattern}{\blueclasscolor}{\secondclassshape}
\classpict{mediumclasssize}{\threeclassnumber}{\fullclasspattern}{\greenclasscolor}{\thirdclassshape}
\classpict{mediumclasssize}{\jokerclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\jokerclassshape}
\end{center}

\newpage
\section{Utilities}\label{sec:util}

Here are various ways to use the package.
It is particularly useful to display \gamename classifications (see \S\ref{sec:trees}).
Please look at the source of this document to understand how they have been produced.

\subsection{Card set}\label{sec:set}

\begin{center}
  \begin{tikzpicture}
    \foreach[count=\i from 0, evaluate=\i as \yy using int(\i*50)] \filling in {\emptyclasspattern,\fillingclasspattern,\fullclasspattern}{
      \foreach[count=\j from 0, evaluate=\j as \y using int(\yy+(\j*15))] \shape in {\firstclassshape,\secondclassshape,\thirdclassshape} {
        \foreach[count=\k from 0, evaluate=\k as \xx using int(\k*50)] \localcolor in {\redclasscolor,\blueclasscolor,\greenclasscolor} {
          \foreach[count=\l from 0, evaluate=\l as \x using int(\xx+(\l*15))] \number in {1,2,3} {
            \classcard{smallclasssize}{\number}{\filling}{\localcolor}{\shape}{\x mm}{-\y mm}
          }
        }
      }
    }
  \end{tikzpicture}
\end{center}

\newpage

% ------------------------------------------------------
% A nice random spread of 81 cards on the page...
% Different each time if seed not used (in principle...)

\begin{tikzpicture}[remember picture, overlay,shift={($(current page.center)$)}]
  \foreach \filling in {\emptyclasspattern,\fillingclasspattern,\fullclasspattern}{
    \foreach \shape in {\firstclassshape,\secondclassshape,\thirdclassshape} {
      \foreach \localcolor in {\redclasscolor,\blueclasscolor,\greenclasscolor} {
        \foreach \number in {1,2,3} {
          % pgfmathsetmacro is the 'only' way to have the value in a variable
          % instead of a string that will be evaluated!
          \pgfmathsetmacro{\rando}{int(rnd*80-40)}%;
          % These values are good  rand*(papersizex-margin*2)-papersize/2-margin
          \pgfmathsetmacro{\randx}{rnd*\textwidth-\textwidth/2}%; % wider: \paperwidth
          \pgfmathsetmacro{\randy}{rnd*\textheight-\textheight/2}%; % wider: \paperheight
          \classcard{thick,scale=.4,rotate=\rando}{\number}{\filling}{\localcolor}{\shape}{\randx}{\randy}%;
        }
      }
    }
  }
\end{tikzpicture}

\subsection{Card set randomly distributed on a page}
% The content is above, otherwise this would not be readable...

Can you count them to be sure the 81 cards are there?

Notice the still vertical stripes\dots

\newpage

\subsection{Class lattice}\label{sec:lattice}

This is the lattice of all classes covering the card \classpict{tinyclasssize}{\twoclassnumber}{\fillingclasspattern}{\redclasscolor}{\secondclassshape}.

\begin{center}
  \begin{tikzpicture}
    \lattice{\twoclassnumber}{\fillingclasspattern}{\redclasscolor}{\secondclassshape}
  \end{tikzpicture}
\end{center}

Obtaining the equivalent for all $3^4-1=80$ other combinations is left as an exercise for the idle reader.

\subsection{Trees}\label{sec:trees}

Now we have everything ready, to display board cards, filled or empty.

By using tikZ-qtree and leaving it decide the sibbling distances:
\begin{center}
\begin{tikzpicture}[scale=.8,edge from parent fork down,
    level distance=2cm,
    every node/.append style={inner sep=0,outer sep=0}]
  \Tree
    [.\node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\jokerclassshape}};
      [.\node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\redclasscolor}{\jokerclassshape}};
        [.\node {\classpict{smallclasssize}{\oneclassnumber}{\jokerclasspattern}{\redclasscolor}{\jokerclassshape}};
          \node {\classpict{smallclasssize}{\oneclassnumber}{\jokerclasspattern}{\redclasscolor}{\firstclassshape}};
          \node {\classpict{smallclasssize}{\oneclassnumber}{\jokerclasspattern}{\redclasscolor}{\secondclassshape}};
          \node {\classpict{smallclasssize}{\oneclassnumber}{\jokerclasspattern}{\redclasscolor}{\thirdclassshape}}; ]
        \node {\classpict{smallclasssize}{\twoclassnumber}{\jokerclasspattern}{\redclasscolor}{\jokerclassshape}};
        [.\node {\classpict{smallclasssize}{\threeclassnumber}{\jokerclasspattern}{\redclasscolor}{\jokerclassshape}};
          \node {\classpict{smallclasssize}{\threeclassnumber}{\emptyclasspattern}{\redclasscolor}{\jokerclassshape}};
          \node {\classpict{smallclasssize}{\threeclassnumber}{\fillingclasspattern}{\redclasscolor}{\jokerclassshape}};
          \node {\classpict{smallclasssize}{\threeclassnumber}{\fullclasspattern}{\redclasscolor}{\jokerclassshape}}; ] ]
    \node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\blueclasscolor}{\jokerclassshape}};
    [.\node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\greenclasscolor}{\jokerclassshape}};
      [.\node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\greenclasscolor}{\firstclassshape}}; 
        \node {\classpict{smallclasssize}{\oneclassnumber}{\jokerclasspattern}{\greenclasscolor}{\firstclassshape}};
        \node {\classpict{smallclasssize}{\twoclassnumber}{\jokerclasspattern}{\greenclasscolor}{\firstclassshape}};
        \node {\classpict{smallclasssize}{\threeclassnumber}{\jokerclasspattern}{\greenclasscolor}{\firstclassshape}}; ]
      \node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\greenclasscolor}{\secondclassshape}};
      \node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\greenclasscolor}{\thirdclassshape}}; ] ]
\end{tikzpicture}
\end{center}

By using tikZ and tuning the sibbling distances:
\begin{center}
\begin{tikzpicture}[edge from parent fork down,level 1/.style={sibling distance=4cm},
      level 2/.style={sibling distance=2cm},
      level 3/.style={sibling distance=1.3cm}, % \cardmininterval
      level distance=2cm,
      every node/.append style={inner sep=0,outer sep=0}]
\node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\jokerclassshape}}
  child {node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\redclasscolor}{\jokerclassshape}}
      child {node {\classpict{smallclasssize}{\oneclassnumber}{\jokerclasspattern}{\redclasscolor}{\jokerclassshape}}
        child {node {\classpict{smallclasssize}{\oneclassnumber}{\jokerclasspattern}{\redclasscolor}{\firstclassshape}}}
        child {node {\classpict{smallclasssize}{\oneclassnumber}{\jokerclasspattern}{\redclasscolor}{\secondclassshape}}}
        child {node {\classpict{smallclasssize}{\oneclassnumber}{\jokerclasspattern}{\redclasscolor}{\thirdclassshape}}}}
      child {node {\classpict{smallclasssize}{\twoclassnumber}{\jokerclasspattern}{\redclasscolor}{\jokerclassshape}}}
      child {node {\classpict{smallclasssize}{\threeclassnumber}{\jokerclasspattern}{\redclasscolor}{\jokerclassshape}}
        child {node {\classpict{smallclasssize}{\threeclassnumber}{\emptyclasspattern}{\redclasscolor}{\jokerclassshape}}}
        child {node {\classpict{smallclasssize}{\threeclassnumber}{\fillingclasspattern}{\redclasscolor}{\jokerclassshape}}}
        child {node {\classpict{smallclasssize}{\threeclassnumber}{\fullclasspattern}{\redclasscolor}{\jokerclassshape}}}}}
  child {node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\blueclasscolor}{\jokerclassshape}}}
  child {node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\greenclasscolor}{\jokerclassshape}}
    child {node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\greenclasscolor}{\firstclassshape}}
      child {node {\classpict{smallclasssize}{\oneclassnumber}{\jokerclasspattern}{\greenclasscolor}{\firstclassshape}}}
      child {node {\classpict{smallclasssize}{\twoclassnumber}{\jokerclasspattern}{\greenclasscolor}{\firstclassshape}}}
      child {node {\classpict{smallclasssize}{\threeclassnumber}{\jokerclasspattern}{\greenclasscolor}{\firstclassshape}}}
  }
    child {node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\greenclasscolor}{\secondclassshape}}}
    child {node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\greenclasscolor}{\thirdclassshape}}}
  };

\end{tikzpicture}
\end{center}

\vspace{2cm}

\begin{center}
\begin{tikzpicture}[scale=1.1,edge from parent fork down,level 1/.style={sibling distance=4cm},
      level 2/.style={sibling distance=1.3cm}, % \cardmininterval
      level 3/.style={sibling distance=1.3cm}, % \cardmininterval
      level distance=2cm,
      every node/.append style={inner sep=0,outer sep=0}]
\node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\jokerclassshape}}
  child {node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\thirdclassshape}}
    child {node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\redclasscolor}{\thirdclassshape}}}
    child {node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\greenclasscolor}{\thirdclassshape}}}
    child {node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\blueclasscolor}{\thirdclassshape}}}
    }
  child {node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\firstclassshape}}
    child {node {\classpict{smallclasssize}{\jokerclassnumber}{\emptyclasspattern}{\jokerclasscolor}{\firstclassshape}}
      child {node {\classpict{smallclasssize}{\oneclassnumber}{\emptyclasspattern}{\jokerclasscolor}{\firstclassshape}}}
      child {node {\classpict{smallclasssize}{\twoclassnumber}{\emptyclasspattern}{\jokerclasscolor}{\firstclassshape}}}
      child {node {\classpict{smallclasssize}{\threeclassnumber}{\emptyclasspattern}{\jokerclasscolor}{\firstclassshape}}}}
    child {node {\classpict{smallclasssize}{\jokerclassnumber}{\fillingclasspattern}{\jokerclasscolor}{\firstclassshape}}}
    child {node {\classpict{smallclasssize}{\jokerclassnumber}{\fullclasspattern}{\jokerclasscolor}{\firstclassshape}}}
    }
  child {node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\secondclassshape}}
  };

\end{tikzpicture}
\end{center}

\vspace{2cm}

These are those that we use for the game boards:

\begin{center}
\begin{tikzpicture}[scale=1.1,edge from parent fork down,level 1/.style={sibling distance=3cm},
      level 2/.style={sibling distance=\cardmininterval},
      level 3/.style={sibling distance=\cardmininterval},
      level distance=2cm,
      every node/.append style={inner sep=0,outer sep=0}]
\node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\jokerclassshape}}
  child {node {\classpict{smallclasssize}{\oneclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\jokerclassshape}}
    child[sibling distance=2.5cm] {node {\classpict{smallclasssize}{\oneclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\secondclassshape}}
      child {node {\classpict{finalclasssize}{\oneclassnumber}{\emptyclasspattern}{\jokerclasscolor}{\secondclassshape}}}
      child {node {\classpict{finalclasssize}{\oneclassnumber}{\fillingclasspattern}{\jokerclasscolor}{\secondclassshape}}}
      child {node {\classpict{finalclasssize}{\oneclassnumber}{\fullclasspattern}{\jokerclasscolor}{\secondclassshape}}}}
    child[sibling distance=2.5cm] {node {\classpict{finalclasssize}{\oneclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\thirdclassshape}}}
    child[sibling distance=2.5cm] {node {\classpict{smallclasssize}{\oneclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\firstclassshape}}
      child {node {\classpict{finalclasssize}{\oneclassnumber}{\emptyclasspattern}{\jokerclasscolor}{\firstclassshape}}}
      child {node {\classpict{finalclasssize}{\oneclassnumber}{\fillingclasspattern}{\jokerclasscolor}{\firstclassshape}}}
      child {node {\classpict{finalclasssize}{\oneclassnumber}{\fullclasspattern}{\jokerclasscolor}{\firstclassshape}}}}
    }
  child {node {\classpict{finalclasssize}{\twoclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\jokerclassshape}}}
  child {node {\classpict{smallclasssize}{\threeclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\jokerclassshape}}
    child {node {\classpict{finalclasssize}{\threeclassnumber}{\jokerclasspattern}{\redclasscolor}{\jokerclassshape}}}
    child {node {\classpict{finalclasssize}{\threeclassnumber}{\jokerclasspattern}{\blueclasscolor}{\jokerclassshape}}}
    child {node {\classpict{finalclasssize}{\threeclassnumber}{\jokerclasspattern}{\greenclasscolor}{\jokerclassshape}}}
  };

\draw (5,1) node {{\tiny 11/3--9}};
\end{tikzpicture}
\end{center}

\vspace{2cm}

Circular trees are not really more legible:

\begin{center}
  % This is for overcomming a bug in tikz-qtree
  % https://tex.stackexchange.com/questions/84528/tikz-qtree-mangles-edge-anchors-in-trees#84534
\begin{tikzpicture}[scale=1.1,
    grow cyclic,
    level 1/.style={sibling angle=127,level distance=1.8cm},
    level 2/.style={sibling angle=60,level distance=1.8cm},
    level 3/.style={sibling angle=45},
    level distance=1.8cm,
    every node/.style={inner sep=0,outer sep=0}
    ]
\tikzset{edge from parent/.style={draw, edge from parent path={(\tikzparentnode) -- (\tikzchildnode)}}}

\draw[dotted] (0,0) circle (1.8cm);
\draw[dotted] (0,0) circle (3.6cm);

\node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\jokerclassshape}}
    child {node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\redclasscolor}{\jokerclassshape}}
      child {node {\classpict{finalclasssize}{\jokerclassnumber}{\emptyclasspattern}{\redclasscolor}{\jokerclassshape}}}
      child {node {\classpict{finalclasssize}{\jokerclassnumber}{\fillingclasspattern}{\redclasscolor}{\jokerclassshape}}}
      child {node {\classpict{finalclasssize}{\jokerclassnumber}{\fullclasspattern}{\redclasscolor}{\jokerclassshape}}}}
    child {node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\blueclasscolor}{\jokerclassshape}}
      child {node {\classpict{finalclasssize}{\oneclassnumber}{\jokerclasspattern}{\blueclasscolor}{\jokerclassshape}}}
      child {node {\classpict{finalclasssize}{\twoclassnumber}{\jokerclasspattern}{\blueclasscolor}{\jokerclassshape}}}
      child {node {\classpict{finalclasssize}{\threeclassnumber}{\jokerclasspattern}{\blueclasscolor}{\jokerclassshape}}}}
  child {node {\classpict{smallclasssize}{\jokerclassnumber}{\jokerclasspattern}{\greenclasscolor}{\jokerclassshape}}
      child {node {\classpict{finalclasssize}{\jokerclassnumber}{\emptyclasspattern}{\greenclasscolor}{\jokerclassshape}}}
      child {node {\classpict{finalclasssize}{\jokerclassnumber}{\fillingclasspattern}{\greenclasscolor}{\jokerclassshape}}}
      child {node {\classpict{finalclasssize}{\jokerclassnumber}{\fullclasspattern}{\greenclasscolor}{\jokerclassshape}}}
  };

\draw (5,1) node {{\tiny 9/2--1}};
\end{tikzpicture}
\end{center}

\begin{center}
\rotatebox{90}{ % coul also use sideways figure
\begin{tikzpicture}[edge from parent fork down,level 1/.style={sibling distance=8.1cm},
      level 2/.style={sibling distance=2.7cm},
      level 3/.style={sibling distance=.9cm},
      level distance=2cm,
      every node/.append style={inner sep=0,outer sep=0}]
\node {\classpict{tinyclasssize}{\jokerclassnumber}{\jokerclasspattern}{\jokerclasscolor}{\jokerclassshape}}
  child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\jokerclasspattern}{\redclasscolor}{\jokerclassshape}}
      child {node {\classpict{tinyclasssize}{\oneclassnumber}{\jokerclasspattern}{\redclasscolor}{\jokerclassshape}}
        child {node {\classpict{tinyclasssize}{\oneclassnumber}{\jokerclasspattern}{\redclasscolor}{\firstclassshape}}}
        child {node {\classpict{tinyclasssize}{\oneclassnumber}{\jokerclasspattern}{\redclasscolor}{\secondclassshape}}}
        child {node {\classpict{tinyclasssize}{\oneclassnumber}{\jokerclasspattern}{\redclasscolor}{\thirdclassshape}}}}
      child {node {\classpict{tinyclasssize}{\twoclassnumber}{\jokerclasspattern}{\redclasscolor}{\jokerclassshape}}
        child {node {\classpict{tinyclasssize}{\twoclassnumber}{\emptyclasspattern}{\redclasscolor}{\jokerclassshape}}}
        child {node {\classpict{tinyclasssize}{\twoclassnumber}{\fillingclasspattern}{\redclasscolor}{\jokerclassshape}}}
        child {node {\classpict{tinyclasssize}{\twoclassnumber}{\fullclasspattern}{\redclasscolor}{\jokerclassshape}}}}
      child {node {\classpict{tinyclasssize}{\threeclassnumber}{\jokerclasspattern}{\redclasscolor}{\jokerclassshape}}
        child {node {\classpict{tinyclasssize}{\threeclassnumber}{\jokerclasspattern}{\redclasscolor}{\firstclassshape}}}
        child {node {\classpict{tinyclasssize}{\threeclassnumber}{\jokerclasspattern}{\redclasscolor}{\secondclassshape}}}
        child {node {\classpict{tinyclasssize}{\threeclassnumber}{\jokerclasspattern}{\redclasscolor}{\thirdclassshape}}}}}
  child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\jokerclasspattern}{\blueclasscolor}{\jokerclassshape}}
      child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\emptyclasspattern}{\blueclasscolor}{\jokerclassshape}}
        child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\emptyclasspattern}{\blueclasscolor}{\firstclassshape}}}
        child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\emptyclasspattern}{\blueclasscolor}{\secondclassshape}}}
        child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\emptyclasspattern}{\blueclasscolor}{\thirdclassshape}}}}
      child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\fillingclasspattern}{\blueclasscolor}{\jokerclassshape}}
        child {node {\classpict{tinyclasssize}{\oneclassnumber}{\fillingclasspattern}{\blueclasscolor}{\jokerclassshape}}}
        child {node {\classpict{tinyclasssize}{\twoclassnumber}{\fillingclasspattern}{\blueclasscolor}{\jokerclassshape}}}
        child {node {\classpict{tinyclasssize}{\threeclassnumber}{\fillingclasspattern}{\blueclasscolor}{\jokerclassshape}}}}
      child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\fullclasspattern}{\blueclasscolor}{\jokerclassshape}}
        child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\fullclasspattern}{\blueclasscolor}{\firstclassshape}}}
        child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\fullclasspattern}{\blueclasscolor}{\secondclassshape}}}
        child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\fullclasspattern}{\blueclasscolor}{\thirdclassshape}}}}}
  child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\jokerclasspattern}{\greenclasscolor}{\jokerclassshape}}
      child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\emptyclasspattern}{\greenclasscolor}{\firstclassshape}}
        child {node {\classpict{tinyclasssize}{\oneclassnumber}{\jokerclasspattern}{\greenclasscolor}{\firstclassshape}}}
        child {node {\classpict{tinyclasssize}{\twoclassnumber}{\jokerclasspattern}{\greenclasscolor}{\firstclassshape}}}
        child {node {\classpict{tinyclasssize}{\threeclassnumber}{\jokerclasspattern}{\greenclasscolor}{\firstclassshape}}}}
      child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\fillingclasspattern}{\greenclasscolor}{\secondclassshape}}
        child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\emptyclasspattern}{\greenclasscolor}{\secondclassshape}}}
        child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\fillingclasspattern}{\greenclasscolor}{\secondclassshape}}}
        child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\fullclasspattern}{\greenclasscolor}{\secondclassshape}}}}
      child {node {\classpict{tinyclasssize}{\jokerclassnumber}{\jokerclasspattern}{\greenclasscolor}{\thirdclassshape}}
        child {node {\classpict{tinyclasssize}{\oneclassnumber}{\jokerclasspattern}{\greenclasscolor}{\thirdclassshape}}}
        child {node {\classpict{tinyclasssize}{\twoclassnumber}{\jokerclasspattern}{\greenclasscolor}{\thirdclassshape}}}
        child {node {\classpict{tinyclasssize}{\threeclassnumber}{\jokerclasspattern}{\greenclasscolor}{\thirdclassshape}}}}
    };
%\end{tikzpicture}

%\begin{tikzpicture}
\begin{scope}[yshift=-8cm]
  \emptyfourleveltree  %; The empty one...
\end{scope}
\end{tikzpicture}}
\thispagestyle{empty}
\end{center}

\subsection{Miscellaneous}

Of course, remain the opportunity to draw a hand of cards as:

\begin{center}
\begin{tikzpicture}
  \path (0,0) .. controls +(1,.5) and +(-1,.5) ..
  node[sloped,pos=0] {\classpict{finalclasssize}{\threeclassnumber}{\emptyclasspattern}{\greenclasscolor}{\secondclassshape}}
%  node[sloped,pos=.1] {\classpict{finalclasssize}{\twoclassnumber}{\fillingclasspattern}{\greenclasscolor}{\firstclassshape}}
  node[sloped,pos=.2] {\classpict{finalclasssize}{\twoclassnumber}{\fillingclasspattern}{\blueclasscolor}{\thirdclassshape}}
%  node[sloped,pos=.3] {\classpict{finalclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{\thirdclassshape}}
  node[sloped,pos=.4] {\classpict{finalclasssize}{\threeclassnumber}{\emptyclasspattern}{\blueclasscolor}{\firstclassshape}}
%  node[sloped,pos=.5] {\classpict{finalclasssize}{\oneclassnumber}{\emptyclasspattern}{\greenclasscolor}{\secondclassshape}}
  node[sloped,pos=.6] {\classpict{finalclasssize}{\oneclassnumber}{\fullclasspattern}{\redclasscolor}{\firstclassshape}}
%  node[sloped,pos=.7] {\classpict{finalclasssize}{\threeclassnumber}{\fullclasspattern}{\blueclasscolor}{\secondclassshape}}
  node[sloped,pos=.8] {\classpict{finalclasssize}{\oneclassnumber}{\fillingclasspattern}{\blueclasscolor}{\firstclassshape}}
%  node[sloped,pos=.9] {\classpict{finalclasssize}{\twoclassnumber}{\fullclasspattern}{\redclasscolor}{\thirdclassshape}}
  node[sloped,pos=1] {\classpict{finalclasssize}{\twoclassnumber}{\fullclasspattern}{\redclasscolor}{\secondclassshape}}
  (4,0);
\end{tikzpicture}
\end{center}

\section{Known issues}

\subsection{standalone}

When converting TikZ pictures with the \texttt{standalone}/\texttt{convert} document class, it is necessary to pass it the \texttt{preview} option, otherwise the result may be problematically trimmed.

\end{document}
